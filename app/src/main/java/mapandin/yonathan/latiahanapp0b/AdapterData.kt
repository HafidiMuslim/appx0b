package mapandin.yonathan.latiahanapp0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterData(val dataSpatu: List<HashMap<String,String>>,
                  val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterData.HolderDataSpatu>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterData.HolderDataSpatu {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_data,p0,false)
        return HolderDataSpatu(v)
    }

    override fun getItemCount(): Int {
        return dataSpatu.size
    }

    override fun onBindViewHolder(p0: AdapterData.HolderDataSpatu, p1: Int) {
        val data = dataSpatu.get(p1)
        p0.txIdMerk.setText(data.get("idMerk"))
        p0.txMerk.setText(data.get("merk"))
        p0.txHarga.setText(data.get("harga"))
        p0.txBrand.setText(data.get("brand"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            val pos = mainActivity.daftarLevel.indexOf(data.get("brand"))
            mainActivity.spLevel.setSelection(pos)
            mainActivity.edIdMerk.setText(data.get("idMerk"))
            mainActivity.edMerk.setText(data.get("merk"))
            mainActivity.edHarga.setText(data.get("brand"))
            Picasso.get().load(data.get("url")).into(mainActivity.imUP)
        })
        //endNew
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.gambar);
    }

    class HolderDataSpatu(v: View) : RecyclerView.ViewHolder(v){
        val txIdMerk = v.findViewById<TextView>(R.id.txIdMerk)
        val txMerk = v.findViewById<TextView>(R.id.txMerk)
        val txHarga = v.findViewById<TextView>(R.id.txHarga)
        val txBrand = v.findViewById<TextView>(R.id.txBrand)
        val gambar = v.findViewById<ImageView>(R.id.imGambar)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}